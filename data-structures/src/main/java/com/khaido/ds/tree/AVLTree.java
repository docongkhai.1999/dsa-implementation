package com.khaido.ds.tree;

public class AVLTree<Key extends Comparable<Key>, Value>
             extends BSTree<Key, Value> {

  @Override
  public void insert(Key key, Value value) {
    if(root == null) {
      root = new AVLNode<>(key, value, 1);
    }
   root = insert(root, key, value);
  }

  @Override
  public void remove(Key key) {
    if(root == null)
      return;
    root = remove (root, key);
  }

  @Override
  public BSTNode<Key, Value> find(Key key) {
    return find(root, key);
  }

  private BSTNode<Key, Value> remove(BSTNode<Key, Value> node, Key key)
  {
    if(key.compareTo(node.getKey()) < 0) {
      var delNode = remove(node.getLeft(), key);
      node.setLeft(delNode);
    }
    else if(key.compareTo(node.getKey()) > 0)
    {
      var delNode = remove(node.getRight(), key);
      node.setRight(delNode);
    }
    else
    {
      if(node.getLeft() == null)
        return node.getRight();
      else if (node.getRight() == null)
        return node.getLeft();
      else
      {
        var replaceNode = findNearestLeft(node.getRight());
        node.setKey(replaceNode.getKey());
        node.setValue(replaceNode.getValue());
        var rightNode = remove(node.getRight(), node.getKey());
        node.setRight(rightNode);
      }
    }

    var avlNode = (AVLNode<Key, Value>) node;
    avlNode.setHeight(height(node) + 1);

    int balance = avlNode.getBalance();

    //left left case
    if(balance > 1 && ((AVLNode<Key, Value>)avlNode.getLeft()).getBalance() >= 0) {
      avlNode = (AVLNode<Key, Value>) rotateRight(avlNode);
      avlNode.setHeight(height(node) + 1);
      ((AVLNode<Key, Value>)avlNode.getLeft()).setHeight(height(avlNode) + 1);
      return avlNode;
    }

    //left right case
    if(balance > 1 && ((AVLNode<Key, Value>)avlNode.getLeft()).getBalance() < 0)
    {
      var rNode = (AVLNode<Key, Value>)rotateLeft(avlNode.getLeft());
      rNode.setHeight(height(rNode) + 1);
      ((AVLNode<Key, Value>)rNode.getLeft()).setHeight(height(avlNode) + 1);
      avlNode.setLeft(rNode);

      avlNode = (AVLNode<Key, Value>) rotateRight(avlNode);
      avlNode.setHeight(height(node) + 1);
      ((AVLNode<Key, Value>)avlNode.getRight()).setHeight(height(avlNode) + 1);
      return avlNode;
    }

    //right right case
    if (balance <- 1 && ((AVLNode<Key, Value>)avlNode.getLeft()).getBalance() <= 0) {
      avlNode = (AVLNode<Key, Value>) rotateLeft(avlNode);
      avlNode.setHeight(height(node) + 1);
      ((AVLNode<Key, Value>)avlNode.getLeft()).setHeight(height(avlNode) + 1);
      return avlNode;
    }

    //right left case
    if(balance <- 1 && ((AVLNode<Key, Value>)avlNode.getLeft()).getBalance() > 0)
    {
      var lNode = (AVLNode<Key, Value>)rotateRight(avlNode.getRight());
      lNode.setHeight(height(lNode) + 1);
      ((AVLNode<Key, Value>)lNode.getRight()).setHeight(height(avlNode) + 1);
      avlNode.setRight(lNode);

      avlNode = (AVLNode<Key, Value>) rotateLeft(avlNode);
      avlNode.setHeight(height(node) + 1);
      ((AVLNode<Key, Value>)avlNode.getLeft()).setHeight(height(avlNode) + 1);
      return avlNode;
    }

    return avlNode;
  }

  private BSTNode<Key, Value> findNearestLeft(BSTNode<Key, Value> node)
  {
    var rsNode = node;
    while(rsNode.getLeft() != null)
      rsNode = rsNode.getLeft();

    return rsNode;
  }

  private BSTNode<Key, Value> insert(BSTNode<Key, Value> node, Key key, Value value)
  {
    if(node == null)
      node = new AVLNode<>(key, value, 1);

    if(key.compareTo(node.getKey()) < 0)
    {
      var newNode = insert(node.getLeft(), key, value);
      node.setLeft(newNode);
    }
    else if (key.compareTo(node.getKey()) > 0) {
      var newNode = insert(node.getRight(), key, value);
      node.setRight(newNode);
    }

    var avlNode = (AVLNode<Key, Value>) node;
    avlNode.setHeight(height(node) + 1);

    int balance = avlNode.getBalance();

    //left left case
    if(balance > 1 && key.compareTo(avlNode.getLeft().getKey()) < 0) {
      avlNode = (AVLNode<Key, Value>) rotateRight(avlNode);
      avlNode.setHeight(height(node) + 1);
      ((AVLNode<Key, Value>)avlNode.getLeft()).setHeight(height(avlNode) + 1);
      return avlNode;
    }

    //left right case
    if(balance > 1 && key.compareTo(avlNode.getLeft().getKey()) > 0)
    {
      var rNode = (AVLNode<Key, Value>)rotateLeft(avlNode.getLeft());
      rNode.setHeight(height(rNode) + 1);
      ((AVLNode<Key, Value>)rNode.getLeft()).setHeight(height(avlNode) + 1);
      avlNode.setLeft(rNode);

      avlNode = (AVLNode<Key, Value>) rotateRight(avlNode);
      avlNode.setHeight(height(node) + 1);
      ((AVLNode<Key, Value>)avlNode.getRight()).setHeight(height(avlNode) + 1);
      return avlNode;
    }

    //right right case
    if (balance <- 1 && key.compareTo(avlNode.getRight().getKey()) > 0) {
      avlNode = (AVLNode<Key, Value>) rotateLeft(avlNode);
      avlNode.setHeight(height(node) + 1);
      ((AVLNode<Key, Value>)avlNode.getLeft()).setHeight(height(avlNode) + 1);
      return avlNode;
    }

    //right left case
    if(balance <- 1 && key.compareTo(avlNode.getRight().getKey()) < 0)
    {
      var lNode = (AVLNode<Key, Value>)rotateRight(avlNode.getRight());
      lNode.setHeight(height(lNode) + 1);
      ((AVLNode<Key, Value>)lNode.getRight()).setHeight(height(avlNode) + 1);
      avlNode.setRight(lNode);

      avlNode = (AVLNode<Key, Value>) rotateLeft(avlNode);
      avlNode.setHeight(height(node) + 1);
      ((AVLNode<Key, Value>)avlNode.getLeft()).setHeight(height(avlNode) + 1);
      return avlNode;
    }

    return avlNode;
  }

  private BSTNode<Key, Value> find(BSTNode<Key, Value> node, Key key)
  {
    if (node == null)
      return null;
    if(key.compareTo(node.getKey()) < 0)
      return find(node.getLeft(), key);
    else if (key.compareTo(node.getKey()) > 0)
      return  find(node.getRight(), key);
    else
      return node;
  }

  public void traverse(TraversalOrder order)
  {
    traverse(order,
             root,
             () -> {
               System.out.print("Traversing : ");
               return 0;
             },
             (node, result) -> {
               System.out.print(node.getValue() + ", ");
               return 0;
             }, node -> {});
    System.out.println("\n");
  }
}
