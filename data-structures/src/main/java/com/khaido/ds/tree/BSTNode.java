package com.khaido.ds.tree;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@AllArgsConstructor
@SuperBuilder
public class BSTNode <Key extends Comparable<Key>, Value> {
  protected Key key;
  protected Value value;
  protected BSTNode<Key, Value> left;
  protected BSTNode<Key, Value> right;

  public BSTNode() {
    left = null;
    right = null;
  }

  public BSTNode(Key key, Value value) {
    super();
    this.key = key;
    this.value = value;
  }

  @Override
  public String toString() {
    return "(" + key +", " + value + ')';
  }
}
