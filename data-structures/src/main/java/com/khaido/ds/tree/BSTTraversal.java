package com.khaido.ds.tree;

import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Supplier;

@FunctionalInterface
public interface BSTTraversal<Key extends Comparable<Key>, Value, T> {
  T traverse(BSTNode<Key, Value> node,
             Supplier<T> constructor,
             BiFunction<BSTNode<Key, Value>, T, T> processor,
             Consumer<BSTNode<Key, Value>> beforeEnqueue);
}
