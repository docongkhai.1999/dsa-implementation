package com.khaido.ds.tree;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
public class AVLNode<Key extends Comparable<Key>, Value> extends BSTNode<Key, Value>{
  private int height;
  @Getter(AccessLevel.NONE)
  private int balance;

  public AVLNode(Key key, Value value, int height) {
    super(key, value);
    this.height = height;
  }
  public AVLNode(Key key, Value value, BSTNode<Key, Value> left, BSTNode<Key, Value> right,
                 int height) {
    super(key, value);
    this.left = left;
    this.right = right;
    this.height = height;
  }

  public int getBalance() {
    int lHeight = left == null ? 0 : ((AVLNode<Key, Value>)left).getHeight();
    int rHeight = right == null ? 0 : ((AVLNode<Key, Value>)right).getHeight();
    return lHeight - rHeight;
  }
}
