package com.khaido.ds.tree;

public class SplayTree<Key extends Comparable<Key>, Value>
              extends BSTree<Key, Value>{

  public void traverse(TraversalOrder order)
  {
    traverse(order,
              root,
             () -> {
                      System.out.print("Traversing : ");
                          return 0;
                          },
             (node, result) -> {
               var splaynode = (SplayNode<Key, Value>) node;
                String parent = splaynode.getParent() == null ?
                                "" :
                                "-parent(" + splaynode.getParent().getValue() + ")";
                System.out.print(node.getValue() + (node == root ? "" : parent) + ", ");
               return 0;
             }, node -> {
                var splaynode = (SplayNode<Key, Value>) node;
                if(splaynode.getLeft() != null)
                  ((SplayNode<Key, Value>)splaynode.getLeft()).setParent(splaynode);
                if(splaynode.getRight() != null)
                  ((SplayNode<Key, Value>)splaynode.getRight()).setParent(splaynode);
            });
    ((SplayNode<Key, Value>)root).setParent(null);
    System.out.println("\n");
  }

  @Override
  public void insert(Key key, Value value)
  {
    if( root == null) {
      root = new SplayNode<>(key, value);
    }

    root = splay(root, key);
    int compare = key.compareTo(root.getKey());

    if(compare < 0)
    {
      SplayNode<Key, Value> newNode = new SplayNode<>(key, value);
      newNode.setLeft(root.getLeft());
      newNode.setRight(root);
      root.setLeft(null);
      root = newNode;
    }
    else if(compare > 0)
    {
      SplayNode<Key, Value> newNode = new SplayNode<>(key, value);
      newNode.setLeft(root);
      newNode.setRight(root.getRight());
      root.setRight(null);
      root = newNode;
    }else
      root.setValue(value);
  }

  @Override
  public void remove(Key key)
  {
    if(root == null)
      return;
    root = splay(root, key);
    if(root.getKey().compareTo(key) == 0)
    {
      if(root.getLeft() == null)
        root = root.getRight();
      else
      {
        var rightSubtree = root.getRight();
        root = root.getLeft();
        splay(root, key);
        root.setRight(rightSubtree);
      }
    }
  }

  @Override
  public BSTNode<Key, Value> find(Key key)
  {
    root = splay(root, key);
    if(root == null)
      return null;

    if(key.compareTo(root.getKey()) == 0)
      return root;
    else
      return null;
  }

  /*
  * This method is special method of splay tree, it tries to change structure of tree
  * to bring the nodes is nearly specific node and specific node up to root
  * It makes least recently nodes on the root of tree.
  * The cost of this method is : O(log(n))
  * */
  private SplayNode<Key, Value> splay(BSTNode<Key, Value> node, Key key)
  {
    if(node == null) return null;

    int compare = key.compareTo(node.getKey());
    if(compare < 0)
    {
      if(node.getLeft() == null) return (SplayNode<Key, Value>)node;

      int compare1 = key.compareTo(node.getLeft().getKey());
      if(compare1 < 0)
      {
        SplayNode<Key, Value> splayNode = splay(node.getLeft().getLeft(), key);
        node.getLeft().setLeft(splayNode);
        if(node.getLeft().getLeft() != null)
          node = rotateRight(node);
      }
      else if (compare1 > 0)
      {
        SplayNode<Key, Value> splayNode = splay(node.getLeft().getRight(), key);
        node.getLeft().setRight(splayNode);
        if(node.getLeft().getRight() != null)
          node.setLeft(rotateLeft(node.getLeft()));
      }

      if(node.getLeft() == null) return (SplayNode<Key, Value>)node;
      else return (SplayNode<Key, Value>)rotateRight(node);
    }
    else if( compare > 0)
    {
      if(node.getRight() == null) return (SplayNode<Key, Value>)node;

      int compare1 = key.compareTo(node.getRight().getKey());
      if(compare1 < 0)
      {
        SplayNode<Key, Value> splayNode = splay(node.getRight().getLeft(), key);
        node.getRight().setLeft(splayNode);
        if(node.getRight().getLeft() != null)
          node.setRight(rotateRight(node.getRight()));
      }
      else if (compare1 > 0)
      {
        SplayNode<Key, Value> splayNode = splay(node.getRight().getRight(), key);
        node.getRight().setRight(splayNode);
        if(node.getRight().getRight() != null)
          node = rotateLeft(node);
      }

      if(node.getRight() == null) return (SplayNode<Key, Value>)node;
      else return (SplayNode<Key, Value>)rotateLeft(node);
    }
    else return (SplayNode<Key, Value>)node;
  }
}