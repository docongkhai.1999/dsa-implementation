package com.khaido.ds.tree;

import java.util.LinkedList;
import java.util.Queue;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Supplier;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class BSTree<Key extends Comparable<Key>, Value> {
    protected BSTNode<Key, Value> root;

    abstract void insert(Key key, Value value);
    abstract void remove(Key key);
    abstract BSTNode<Key, Value> find(Key key);

    public int size(){
        return traverse(TraversalOrder.PREORDER,
                        root, () -> 0,
                        (x, result) -> result + 1,
                        (x) -> {});
    }
    public int height()
    {
        return height(root);
    }

    protected int height(BSTNode<Key, Value> node)
    {
        int height = 0;
        if(node == null)
            return  height;

        Queue<BSTNode<Key, Value>> childrenNodeQueue = new LinkedList<>();
        if(node.getLeft() != null)
            childrenNodeQueue.add(node.getLeft());
        if(node.getRight() != null)
            childrenNodeQueue.add(node.getRight());

        while (true)
        {
            int childNodeCount = childrenNodeQueue.size();
            if(childNodeCount == 0) {
                return height;
            }

            while (childNodeCount > 0)
            {
                BSTNode<Key, Value> curnode = childrenNodeQueue.poll();
                if(curnode.getLeft() != null)
                    childrenNodeQueue.add(curnode.getLeft());
                if(curnode.getRight() != null)
                    childrenNodeQueue.add(curnode.getRight());
                childNodeCount --;
            }

            height ++;
        }
    }

    protected BSTNode<Key, Value> rotateRight(BSTNode<Key, Value> node)
    {
        BSTNode<Key, Value> nodeLeft = node.getLeft();
        node.setLeft(nodeLeft.getRight());
        nodeLeft.setRight(node);
        return nodeLeft;
    }
    protected BSTNode<Key, Value> rotateLeft(BSTNode<Key, Value> node)
    {
        BSTNode<Key, Value> nodeRight = node.getRight();
        node.setRight(nodeRight.getLeft());
        nodeRight.setLeft(node);
        return nodeRight;
    }

    @SuppressWarnings("unchecked")
    protected <T> T traverse (TraversalOrder order,
                              BSTNode<Key, Value> node,
                              Supplier<T> constructor,
                              BiFunction<BSTNode<Key, Value>, T, T> processor,
                              Consumer<BSTNode<Key, Value>> beforeEnqueue){
        switch (order)
        {
            case PREORDER:
                return (T) BSTTraversalImps.getInstance()
                    .preorderTraversal
                    .traverse(node, constructor, processor, beforeEnqueue);

            case INORDER:
                return (T) BSTTraversalImps.getInstance()
                    .inorderTraversal
                    .traverse(node, constructor, processor, beforeEnqueue);

            case POSTORDER:
                return (T) BSTTraversalImps.getInstance()
                    .postorderTraversal
                    .traverse(node, constructor, processor, beforeEnqueue);
            default:
                return constructor.get();
        }
    }
}
