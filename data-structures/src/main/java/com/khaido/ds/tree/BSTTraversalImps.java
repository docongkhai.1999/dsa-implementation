package com.khaido.ds.tree;

import java.util.Stack;

public class BSTTraversalImps<Key extends Comparable<Key>, Value> {

  private BSTTraversalImps()
  {

  }

  private static class BSTTraversalHelper{
    public static BSTTraversalImps INSTANCE = new BSTTraversalImps<>();
  }

  public static BSTTraversalImps getInstance()
  {
    return BSTTraversalHelper.INSTANCE;
  }

  @SuppressWarnings("unchecked")
  public BSTTraversal preorderTraversal = (node, constructor, processor, beforeEnqueue) ->{
    var result = constructor.get();
    Stack<BSTNode<Key, Value>> stack = new Stack<>();
    stack.add(node);
    while (!stack.isEmpty())
    {
      BSTNode<Key, Value> curNode = stack.pop();
      result = processor.apply(curNode, result);

      if(curNode.getRight() != null) {
        beforeEnqueue.accept(curNode);
        stack.add(curNode.getRight());
      }

      if(curNode.getLeft() != null) {
        beforeEnqueue.accept(curNode);
        stack.add(curNode.getLeft());
      }
    }
    return result;
  };

  @SuppressWarnings("unchecked")
  public BSTTraversal inorderTraversal = (node, constructor, processor, beforeEnqueue) ->{
    var result = constructor.get();
    Stack<BSTNode<Key, Value>> stack = new Stack<>();
    var curNode = node;
    while (!stack.isEmpty() || curNode != null)
    {
      while (curNode != null)
      {
        stack.add(curNode);

        if(curNode.getLeft() != null) {
          beforeEnqueue.accept(curNode);
        }
        curNode = curNode.getLeft();
      }

      curNode = stack.pop();
      result = processor.apply(curNode, result);
      curNode = curNode.getRight();
    }
    return result;
  };

  @SuppressWarnings("unchecked")
  public BSTTraversal postorderTraversal = (node, constructor, processor, beforeEnqueue) ->{
    var result = constructor.get();
    Stack<BSTNode<Key, Value>> traversalStack = new Stack<>();
    traversalStack.add(node);

    Stack<BSTNode<Key, Value>> resultStack = new Stack<>();
    while (!traversalStack.isEmpty())
    {
      BSTNode<Key, Value> curNode = traversalStack.pop();
      resultStack.add(curNode);

      if(curNode.getLeft() != null) {
        beforeEnqueue.accept(curNode);
        traversalStack.add(curNode.getLeft());
      }

      if(curNode.getRight() != null) {
        beforeEnqueue.accept(curNode);
        traversalStack.add(curNode.getRight());
      }
    }

    while (!resultStack.isEmpty()) {
      var curNode = resultStack.pop();
      result = processor.apply(curNode, result);
    }

    return result;
  };

}
