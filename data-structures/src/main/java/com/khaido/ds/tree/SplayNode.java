package com.khaido.ds.tree;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
public class SplayNode <Key extends Comparable<Key>, Value> extends BSTNode<Key, Value> {
  private SplayNode<Key, Value> parent;
  public SplayNode(Key key, Value value) {
    super(key,value);
  }

  public SplayNode(Key key, Value value,
                   BSTNode<Key, Value> left,
                   BSTNode<Key, Value> right,
                   SplayNode<Key, Value> parent) {
    super(key, value, left, right);
    this.parent = parent;
  }
}