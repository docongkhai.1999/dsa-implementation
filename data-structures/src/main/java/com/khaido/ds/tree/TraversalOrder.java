package com.khaido.ds.tree;

public enum TraversalOrder {
  PREORDER,
  INORDER,
  POSTORDER
}
