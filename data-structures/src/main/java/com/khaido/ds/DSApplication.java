package com.khaido.ds;

import com.khaido.ds.tree.AVLTree;
import com.khaido.ds.tree.TraversalOrder;

public class DSApplication {
  public static void main(String[] args)
  {

    /*
    //************** SPLAY TREE *************
    SplayTree<Integer, Integer> tree = new SplayTree<>();
    tree.insert(1, 1);
    tree.insert(2, 2);
    tree.insert(3, 3);
    tree.insert(4, 4);
    tree.insert(5, 5);
    tree.insert(6, 6);
    tree.insert(7, 7);
    tree.insert(8, 8);
    tree.insert(9, 9);

    System.out.println("Size : " + tree.size());
    System.out.println("Height : " + tree.height());
    tree.traverse(TraversalOrder.PREORDER);
    System.out.println("Find 5 : " + tree.find(5).toString());
    System.out.println("Traversal PreOrder : " + tree.height());
    tree.traverse(TraversalOrder.PREORDER);
    System.out.println("Traversal InOrder : " + tree.height());
    tree.traverse(TraversalOrder.INORDER);
    System.out.println("Traversal PostOrder : " + tree.height());
    tree.traverse(TraversalOrder.POSTORDER);
    System.out.println("Height : " + tree.height());
    */

    //************* AVL TREE  *************
    AVLTree<Integer, Integer> avlTree = new AVLTree<>();
    avlTree.insert(1, 1);
    avlTree.insert(2, 2);
    avlTree.insert(3, 3);
    avlTree.insert(4, 4);
    avlTree.insert(5, 5);
    avlTree.insert(6, 6);
    avlTree.insert(7, 7);
    avlTree.insert(8, 8);
    avlTree.insert(9, 9);

    System.out.println("Size : " + avlTree.size());
    System.out.println("Height : " + avlTree.height());
    System.out.println("root : " + avlTree.getRoot().toString());
    avlTree.traverse(TraversalOrder.PREORDER);
    System.out.println("Find : "+ avlTree.find(5));
    avlTree.remove(6);
    System.out.println("After del 6 : ");
    avlTree.traverse(TraversalOrder.PREORDER);
  }
}